/*
 * sdr_host_interface_io.h
 *
 *  Created on: 19 янв. 2018 г.
 *      Author: svetozar
 */

#ifndef SDR_HOST_INTERFACE_INTERNAL_IO_H_
#define SDR_HOST_INTERFACE_INTERNAL_IO_H_

#include "../io.h"

#ifdef __cplusplus
extern "C"{
#endif

extern Size_t HOST_IO_raw_send_buf(HOST_IO_t * This, const UInt8_t * Buf, const Size_t Count);

typedef enum
{
  host_io_OK = 0,

  host_io_FAIL = -0x10,
  host_io_FAIL_CRC = -0x11
} HOST_IO_Result_t;

HOST_IO_Result_t HOST_IO_raw_rx_buf_handler(HOST_IO_t * This, UInt8_t * data, const Size_t count);

#ifdef __cplusplus
}
#endif

#endif /* SDR_HOST_INTERFACE_INTERNAL_IO_H_ */
