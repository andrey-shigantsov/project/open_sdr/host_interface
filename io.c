/*
 * io.c
 *
 *  Created on: 18 янв. 2018 г.
 *      Author: svetozar
 */

#include "io.h"
#include "base/io.h"
#include "internal/io.h"

#include <SDR/BASE/Multiplex/common.h>

void init_HOST_IO(HOST_IO_t * This, HOST_IO_Cfg_t * Cfg)
{
#if SDR_SAMPLE_SIZE == 32
  SDR_ASSERT(sizeof(Sample_t) == 4);
#endif

  This->cfg = *Cfg;

  This->Parent = 0;
  This->cbk.rx_UserData_ready = 0;

  symMultiplexCfg_t cfgMux;
  cfgMux.Buf.P = This->txMuxBuf;
  cfgMux.Buf.Size = SDR_HOST_IO_MUX_BUFSIZE;
  cfgMux.symSize = 8;
  init_symMultiplex(&This->txMux, &cfgMux);

  This->isStarted = 0;
  This->isRunning = 0;
}

/*--------------------------------------------------------------------------------------*/

static Bool_t mux_prepare_for_write(HOST_IO_t * This, TARGET_DataId_t id)
{
  if (!IO_Mux_prepare_for_write(&This->txMux)) return false;
  return sym_multiplex_write_uint8(&This->txMux, id, 8);
}

static Bool_t send_from_mux(HOST_IO_t * This)
{
  if (!IO_Mux_prepare_for_send(&This->txMux)) return false;
  Size_t size = symMultiplex_Count(&This->txMux);
  return HOST_IO_raw_send_buf(This, symMultiplex_Data(&This->txMux), size) == size;
}

Bool_t HOST_IO_send_uint8(HOST_IO_t * This, TARGET_DataId_t id, UInt8_t data, Size_t bitsCount)
{
  if (!mux_prepare_for_write(This, id)) return false;
  if (!sym_multiplex_write_uint8(&This->txMux, data, bitsCount)) return false;
  return send_from_mux(This);
}

Bool_t HOST_IO_send_data(HOST_IO_t * This, TARGET_DataId_t id, UInt8_t * Buf, Size_t Count)
{
  if (!mux_prepare_for_write(This, id)) return false;
  if (!sym_multiplex_write_uint32(&This->txMux, Count, 32)) return false;
  Size_t n = sym_multiplex_write_buf(&This->txMux, Buf, Count);
  if (n != Count) return false;
  return send_from_mux(This);
}

Bool_t HOST_IO_send_response(HOST_IO_t * This, TARGET_Response_t Resp)
{
  if (!mux_prepare_for_write(This, TARGET_ResponseId)) return false;
  if (!sym_multiplex_write_uint8(&This->txMux, Resp, 8)) return false;
  return send_from_mux(This);
}

Bool_t HOST_IO_send_state_response(HOST_IO_t * This, TARGET_State_t State)
{
  if (!mux_prepare_for_write(This, TARGET_ResponseId)) return false;
  if (!sym_multiplex_write_uint8(&This->txMux, TARGET_respSTATE, 8)) return false;
  if (!sym_multiplex_write_uint8(&This->txMux, State, 8)) return false;
  return send_from_mux(This);
}

Bool_t HOST_IO_send_params(HOST_IO_t * This, TARGET_Params_t * params)
{
  if (!mux_prepare_for_write(This, TARGET_ResponseId)) return false;
  if (!sym_multiplex_write_uint8(&This->txMux, TARGET_Params, 8)) return false;
  if (!sym_multiplex_write_uint32(&This->txMux, params->timeout_ms,32)) return false;
  return send_from_mux(This);
}

Bool_t HOST_IO_send_name(HOST_IO_t * This, const char * name)
{
  if (!mux_prepare_for_write(This, TARGET_ResponseId)) return false;
  if (!sym_multiplex_write_uint8(&This->txMux, TARGET_NAME, 8)) return false;
  if (!sym_multiplex_write_uint16(&This->txMux, strlen(name), 16)) return false;
  if (!sym_multiplex_write_str(&This->txMux, name, strlen(name))) return false;
  return send_from_mux(This);
}

Bool_t HOST_IO_send_format(HOST_IO_t * This, TARGET_DataId_t id, const char * format)
{
  if (!mux_prepare_for_write(This, TARGET_ResponseId)) return false;
  if (!sym_multiplex_write_uint8(&This->txMux, id, 8)) return false;
  if (!sym_multiplex_write_uint16(&This->txMux, strlen(format), 16)) return false;
  if (!sym_multiplex_write_str(&This->txMux, format, strlen(format))) return false;
  return send_from_mux(This);
}

Bool_t HOST_IO_send_element_value(HOST_IO_t * This, TARGET_ElementType_t type, TARGET_DataId_t id, const char * val)
{
  if (!mux_prepare_for_write(This, TARGET_ElementCommandId)) return false;
  if (!sym_multiplex_write_uint8(&This->txMux, TARGET_elemCmd_setValue, 8)) return false;
  if (!sym_multiplex_write_uint8(&This->txMux, type, 8)) return false;
  if (!sym_multiplex_write_uint8(&This->txMux, id, 8)) return false;
  if (!sym_multiplex_write_uint16(&This->txMux, strlen(val), 16)) return false;
  if (!sym_multiplex_write_str(&This->txMux, val, strlen(val))) return false;
  return send_from_mux(This);
}

Bool_t HOST_IO_send_element_params(HOST_IO_t * This, TARGET_ElementType_t type, TARGET_DataId_t id, const char * params)
{
  if (!mux_prepare_for_write(This, TARGET_ElementCommandId)) return false;
  if (!sym_multiplex_write_uint8(&This->txMux, TARGET_elemCmd_setParams, 8)) return false;
  if (!sym_multiplex_write_uint8(&This->txMux, type, 8)) return false;
  if (!sym_multiplex_write_uint8(&This->txMux, id, 8)) return false;
  if (!sym_multiplex_write_uint16(&This->txMux, strlen(params), 16)) return false;
  if (!sym_multiplex_write_str(&This->txMux, params, strlen(params))) return false;
  return send_from_mux(This);
}

Bool_t HOST_IO_send_plot_command(HOST_IO_t * This, TARGET_DataId_t plotId, TARGET_PlotCommand_t cmd)
{
  if (!mux_prepare_for_write(This, TARGET_PlotCommandId)) return false;
  if (!sym_multiplex_write_uint8(&This->txMux, plotId, 8)) return false;
  if (!sym_multiplex_write_uint8(&This->txMux, cmd, 8)) return false;
  return send_from_mux(This);
}

Bool_t HOST_IO_send_plot_command_ext(HOST_IO_t * This, TARGET_DataId_t plotId, TARGET_PlotCommand_t cmd, const char * ext)
{
  if (!mux_prepare_for_write(This, TARGET_PlotCommandId)) return false;
  if (!sym_multiplex_write_uint8(&This->txMux, plotId, 8)) return false;
  if (!sym_multiplex_write_uint8(&This->txMux, cmd, 8)) return false;
  if (!sym_multiplex_write_uint16(&This->txMux, strlen(ext), 16)) return false;
  if (!sym_multiplex_write_str(&This->txMux, ext, strlen(ext))) return false;
  return send_from_mux(This);
}

Bool_t HOST_IO_send_log_message(HOST_IO_t * This, const char * message)
{
  if (!mux_prepare_for_write(This, TARGET_ResponseId)) return false;
  if (!sym_multiplex_write_uint8(&This->txMux, TARGET_LogMsg, 8)) return false;
  if (!sym_multiplex_write_uint16(&This->txMux, strlen(message), 16)) return false;
  if (!sym_multiplex_write_str(&This->txMux, message, strlen(message))) return false;
  return send_from_mux(This);
}

#if SDR_SAMPLE_SIZE == 32
#define SAMPLE_to_UInt32(x) *(UInt32_t*)&(x)
#else
#error "HOST_IO: SAMPLE_to_UInt32 need implement"
#endif

Bool_t HOST_IO_send_samples(HOST_IO_t * This, TARGET_DataId_t id, Sample_t * Buf, Size_t Count)
{
  if (!mux_prepare_for_write(This, TARGET_SamplesDataId)) return false;
  if (!sym_multiplex_write_uint8(&This->txMux, id, 8)) return false;
  if (!sym_multiplex_write_uint32(&This->txMux, Count, 32)) return false;
  for (Size_t i = 0; i < Count; ++i)
    if(!sym_multiplex_write_uint32(&This->txMux, SAMPLE_to_UInt32(Buf[i]), 32)) return false;
  return send_from_mux(This);
}

Bool_t HOST_IO_send_samples_iq(HOST_IO_t * This, TARGET_DataId_t id, iqSample_t * Buf, Size_t Count)
{
  if (!mux_prepare_for_write(This, TARGET_iqSamplesDataId)) return false;
  if (!sym_multiplex_write_uint8(&This->txMux, id, 8)) return false;
  if (!sym_multiplex_write_uint32(&This->txMux, Count, 32)) return false;
  for (Size_t i = 0; i < Count; ++i)
  {
    if(!sym_multiplex_write_uint32(&This->txMux, SAMPLE_to_UInt32(Buf[i].i), 32)) return false;
    if(!sym_multiplex_write_uint32(&This->txMux, SAMPLE_to_UInt32(Buf[i].q), 32)) return false;
  }
  return send_from_mux(This);
}

/*--------------------------------------------------------------------------------------*/

static HOST_IO_Result_t mux_prepare_for_read(HOST_IO_t * This, UInt8_t * data, Size_t count, HOST_DataId_t * id, UInt16_t * size, Bool_t * crcOk)
{
  symMultiplexCfg_t cfgMux;
  cfgMux.Buf.P = data;
  cfgMux.Buf.Size = count;
  cfgMux.symSize = 8;
  init_symMultiplex(&This->rxMux, &cfgMux);
  symMultiplex_setCount(&This->rxMux, count);

  if (!IO_Mux_prepare_for_read(&This->rxMux, size, crcOk)) return host_io_FAIL;

  if (!sym_multiplex_read_uint8(&This->rxMux, id, 8)) return host_io_FAIL;
  return host_io_OK;
}

HOST_IO_Result_t HOST_IO_raw_rx_buf_handler(HOST_IO_t * This, UInt8_t * data, const Size_t count)
{
  UInt16_t pduSize = 0;
  Bool_t crcOk = true;
  HOST_DataId_t id = 0;

  HOST_IO_Result_t res;

  res = mux_prepare_for_read(This, data, count, &id, &pduSize, &crcOk);
  if (res != host_io_OK)
    return res;

  if (!crcOk)
    return host_io_FAIL_CRC;

  switch (id)
  {
  case HOST_SetParamsId:{
    TARGET_Params_t params;
    if (!sym_multiplex_read_uint32(&This->rxMux, &params.timeout_ms, 32)) return host_io_FAIL;
    if (This->cfg.on_params_changed)
      This->cfg.on_params_changed(This->cfg.Master, &params);
    HOST_IO_send_response(This, TARGET_respOK);
    break; }
  case HOST_CommandId:{
    UInt8_t cmd;
    if (!sym_multiplex_read_uint8(&This->rxMux, &cmd, 8)) return host_io_FAIL;
    switch (cmd)
    {
    case HOST_getState:
      HOST_IO_send_state_response(This, HOST_IO_getState(This));
      break;
    case HOST_cmdSTART:
      HOST_IO_start(This);
      break;
    case HOST_cmdSTEP:
      HOST_IO_step(This);
      break;
    case HOST_cmdPAUSE:
      HOST_IO_pause(This);
      break;
    case HOST_cmdCONTINUE:
      HOST_IO_continue(This);
      break;
    case HOST_cmdRESET:
      HOST_IO_reset(This);
      break;
    case HOST_cmdSTOP:
      HOST_IO_stop(This);
      break;
    case HOST_getAll:
      if (This->cfg.on_get_all)
        This->cfg.on_get_all(This->cfg.Master);
      break;
    case HOST_getFormat:
      if (This->cfg.on_format_request)
        This->cfg.on_format_request(This->cfg.Master);
      break;
    case HOST_setValue:{
      HOST_DataId_t valId;
      if (!sym_multiplex_read_uint8(&This->rxMux, &valId, 8)) return host_io_FAIL;
      UInt16_t len;
      if (!sym_multiplex_read_uint16(&This->rxMux, &len, 16)) return host_io_FAIL;
      if (len)
      {
        if (!sym_multiplex_read_str(&This->rxMux, (char*)This->rxTmpBuf, len)) return host_io_FAIL;
      }
      else This->rxTmpBuf[0] = '\0';
      if (This->cfg.on_set_value)
        This->cfg.on_set_value(This->cfg.Master, valId, (char*)This->rxTmpBuf, len);
      break;}
    }
    break;}
  default:
    if (This->cbk.rx_UserData_ready)
      This->cbk.rx_UserData_ready(This->Parent, id, data+SDR_IO_MUX_HEADER_SIZE+1, pduSize-SDR_IO_MUX_HEADER_SIZE-1);
    break;
  }
  return host_io_OK;
}
